1 + 2
val x = 3
def f(x: Int) = x + 1
f(7)
val l = List(1, 2, 3)
l.length

val romme = """romme"""

val oben = "+---+---+"
val mitte = """
|   |   |
|   |   |
|   |   |
"""
val unten = "+---+---+"

val plus = "+"
val minus = "-"

val zusammen = plus + minus * 3 + plus + minus * 3 + plus

def testfunktion(breite: Int) {
  minus * breite
}

val test = testfunktion(4)

val stern = "*"

val drei = 3
